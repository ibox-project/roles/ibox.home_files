home_files
=========

Manage home files, placed in a directory of the ansible controller, for the ansible user. 

Requirements
------------

None.

Role Variables
--------------

| Variable                           | Required | Description                                                 | Default         |
|------------------------------------|----------|-------------------------------------------------------------|-----------------|
| home_files__path                   | true     | Absolute path to the files, which will be copied.           |                 |
| home_files__owner                  | false    | The new owner, if the existing is not appropriate.          | ansible_user_id |
| home_files__group                  | false    | The new group, if the existing is not appropriate.          | id -gn          | 
| home_files__default_file_mode      | false    | The new mode for files, if the existing is not appropriate. |                 |
| home_files_default_directory_mode  | false    | The new mode for files, if the existing is not appropriate. | 0750            |

Dependencies
------------

```yaml
  - src: https://gitlab.com/ibox-project/roles/ibox.configuration.git
    version: stable
    scm: git
    name: ibox.configuration
```

Example Playbook
----------------
Include the role in `requirements.yml` with something like
```yaml
  - src: https://gitlab.com/ibox-project/roles/home_files.git
    version: main
    scm: git
    name: ibox.home_files
```

and write in the playbook something like

```yaml
  tasks:
    - name: "Include ibox.home_files"
      ansible.builtin.include_role:
        name: "ibox.home_files"
      vars:
        home_files__path: '/Users/macos/projects/home_file_templates'
        home_files__owner: 'macos' # optional
        home_files__default_file_mode: '0640' # optional
        home_files_default_directory_mode: '0750' # optional
```

License
-------

Licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
